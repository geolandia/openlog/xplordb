#!/bin/bash


for f in $(jq '.pages[] | {title} | flatten' < xdb.json| grep -v '\[' | grep -v '\]' | xargs)
do
    echo "File: $f"
    string=$(jq ".pages[] | select(.title == \"$f\") | {text} | flatten " < xdb.json | head -n -1 | tail -n -1 | sed -s 's/^ *"\(.*\)"$/\1/g')
    echo -e "$string" > "$f.md"
done
