# Default xplordb schemas

* public
* assay
* dh (drill hole)
* d (simple drill hole schema) (not impllimented in version 0.87)
* dem (digital elevation model)
* qa (lab assay results quality assurance)
* ref (reference tables)
* surf (surface samples)
* v (validation views)

The public schema is created by default when PostgreSQL is installed, PostgreSQL/PostGIS system functions are stored in the public schema.
  
Further schemas could make other data such as GIS data easier to organise. A general gis schema could be used with systematic table naming or addtional schemas can further subdivide the data by theme or project, eg.

* tenements
* geophysics
* geology
* block models
  
Using schemas may also make backing up data more effcient, whereby a schema with read-only static data may not need regular backups. Or a less used schema may need less backing up etc.

If a number of people use the system a schema for each can be created as a staging or personal use area before potentially merging data into other schema.

The "d" simple drill hole schema (testing for version 0.88) is an unconstrained and simplified version of xplordb as possible.
Enter data in the following order:

1. d.coll - 3D point locations (WGS84) will be automatically produced in the point geometry column. The SRID of the location needs to be entered. The SRID can be found in public.spatial_ref_sys as part of the PostGIS reference tables, or in QGIS.
1. d.surv - a 3D string will automatically be produced in the line geometry column in d.coll. The first survey should be the collar i.e. 0 (zero depth)
1. d.lith - a 3D string will automatically be produced for each interval/row
1. etc. for additional interval or point tables
