# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 0.13.8 - 2025-02-26
- role refactoring : xdb_viewer is read-only, xdb_logger can select, insert, update, xdb_admin can create tables, alter metadata select, insert, update, delete on tables

## 0.13.7 - 2025-02-11

- add of geometries in original SRID : dh.collar.proj_geom, dh.collar.proj_planned_loc, dh.collar.proj_geom_trace, dh.collar.proj_planned_trace, display.display_collar.prof_effective_geom, display.display_collar.prof_planned_geom
- support for geographic SRID (use 3857 as pivot SRID for 3D operations)

## 0.13.6 - 2024-11-08

- add of planned_x, planned_y and planned_z columns to dh.collar table

## 0.13.5 - 2024-09-05

- implementation of display.ST_3DLineSubstring function

## 0.13.4 - 2024-04-25  

- redefinition of structural types (add polarity)
- definition of sqlalchemy classes for using structural types

## 0.13.3 - 2024-04-17  

- creation of dh.metadata table and update datamodel
- trigger to synchronize dh.metadata with dh.collar table
- add unit test about metadata import
- custom types definition for structural data support (in assay schema) : azimuth, dip, kind and spherical_data  

## 0.13.2 - 2024-03-21

- add 5 columns to dh.collar : project_srid, dip, azimuth, planned_eoh, planned_trace (geometry)
- new schema "display" with a new table "display_collar". Used for displaying purpose
- new functions : dh.dh_planned_trace.sql, display.display_effective_trace.sql and display.display_planned_trace.sql  
- optimization of desurveying functions
- trigger optimization (notably desurveying following for dh.surv changes)
- new triggers to maintain display.display_collar up-to-date with dh.collar

## 0.13.01- 2023-02-09

- fix collar views (`dh.collar_view` / `dh.collar_view_geom` / `dh.collar_view_geom_trace`).

## 0.13.0 - 2022-09-29

- release to official pypi repository to avoid error in pypitest repository

## 0.12.5 - 2022-09-29

- new release because there was an issue when deploying to pypi test. No archive available at https://test.pypi.org/simple/xplordb/

## 0.12.4 - 2022-09-29

- add session flush after lith code insertion to avoid foreign key error with sqlalchemy session

## 0.12.3 - 2022-09-23

### Changed
- update privilege for admin and importer on all schemas

## 0.12.2 - 2022-05-17

### Changed
- update collar views (`dh.collar_view` / `dh.collar_view_geom` / `dh.collar_view_geom_trace`) to use survey with minimum depth instead of 0.0 depth.
- desurveying can be done without survey at 0.0 depth. Survey with minimum depth is used for 0.0 depth.

## 0.12.1 - 2022-03-23

### Changed
- fix survey depth column definition for `XplordbSurveyTable` sqlalchemy table definition : must return float and not decimal
- fix lith depth column definition for `XplordbLithTable` sqlalchemy table definition : must return float and not decimal

## 0.12.0 - 2022-03-18

### Added 
- update `ImportData` class to use an `sqlalchemy` connection instead of a `pyscopg2` connection
- use of sqlalchemy base class describing Xplordb tables : new dependency on `sqlalchemy` and `GeoAlchemy2`
- when collar geometry is updated, collar (x,y,z) columns are updated

### Changed
- fix collar delete from database
- update limitation on lithology code to 50 characters
- speedup sonarcloud analysis

## 0.11.0 - 2022-02-24

### Added 
- Add default roles (`xdb_viewer`, `xdb_logger`, `xdb_importer`, `xdb_admin`) and associated grant
- Add End Of Hole support for `collar`
- Add survey_date support for `collar` import
- Add `assay` schema to lite version of xplordb
- Add xplordb database creation with xplordb python module

## 0.10.2 - 2022-02-16
Update python supported version (3.8, 3.9, 3.10)

### Changed
- Update python supported version (3.8, 3.9, 3.10)

## 0.10.1 - 2022-02-15
Fix release for pypi-test repository.

### Changed
- Fix release on pypi-test repository 

## 0.10.0 - 2022-02-11

### Added 
- Add lith table to lite version
- Add `datamodel` module (simple representation of Person / Dataset / Collar / Survey / Lithology)
- Add `ImportData` class to import data into an existing xplordb database
- First version of xplord-0.10.0 pip package on [test.pypi repository](https://test.pypi.org/) and gitlab xplordb package registry

### Changed
- Review of xplordb schema to remove some NOT NULL constraints on columns (data_source)
- Remove hard coded value for survey depth check. Check is done with `dh.details` table use.

### Removed
- Removed original xplordb dump from repository


## 0.9.0 - 2022-02-02

- Split of xplordb dump into separate .sql files
- Add python module xplordb : ImportDDL class used to import xplordb schema on an existing table
- Add tests for minimum curvature method desurveing
- Add lite version of xplordb with minimal schemas for desurveing
- Remove of some foreign keys and not null constraints
