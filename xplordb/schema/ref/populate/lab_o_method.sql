----------------------------------------------------------------------------
-- xplordb
-- 
-- Copyright (C) 2022  Oslandia / OpenLog
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as published
-- by the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
-- 
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
-- 
-- __authors__ = ["davidms"]
-- __contact__ = "geology@oslandia.com"
-- __date__ = "2022/02/02"
-- __license__ = "AGPLv3"
----------------------------------------------------------------------------

--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5 (Ubuntu 13.5-0ubuntu0.21.10.1)
-- Dumped by pg_dump version 13.5 (Ubuntu 13.5-0ubuntu0.21.10.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: lab_o_method; Type: TABLE DATA; Schema: ref; Owner: postgres
--

COPY ref.lab_o_method (code, description, loaded_by, load_date) FROM stdin;
fire	Fire Assay	fp	2016-08-03
ar	Aqua Regia	fp	2016-08-03
li_bor_fus	Lithium Borate Fusion	fp	2016-08-03
four_acid	Four Acid Digest	fp	2016-08-03
fusion	Various Fusion methods	fp	2016-08-03
pxrf	Portable/ Handheld XRF	fp	2016-08-03
u	unknown	fp	2016-08-03
leco	Leco	fp	2016-08-03
ic	Infrared Combustion	fp	2016-08-03
three_acid	Three Acid Digest	fp	2016-08-03
pellet_XRF	Pressed Pellet XRF	fp	2016-08-03
blank	Blank	fp	2016-08-03
weigh	Weight	fp	2016-08-03
\.


--
-- PostgreSQL database dump complete
--

